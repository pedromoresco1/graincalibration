'''
This class is the bulk density test.
The goal is to measure the mass of product in a given volume
and then to calculate the needed specific weight for the particles 
to match the prescribed weight.
'''
import os
import math
import warnings
from exp_proc import ExpProc

################################################################################

class BulkDensity(ExpProc):
#TODO cleanup std values
    def __init__(self, target_density: float, cube_vol: float=1, 
                paddle_speed: float=0.2, particle_radi: float=1/8, 
                particle_ym: float=1e8, particle_pc:float=0.25, 
                liggghts_path: str='', output_folder: str='', rname: str='', 
                out_format: str='vtk', cli: bool = True):
        super().__init__(particle_radi=particle_radi, particle_ym=particle_ym, 
                         particle_pc=particle_pc, liggghts_path=liggghts_path, 
                         output_folder=output_folder, out_format=out_format, 
                         cli=cli)
        self.rname = rname if rname != '' else self.tmp_study_name()
        self.template_dir = os.path.join([
                            os.path.dirname(os.path.realpath(__file__)), 
                            'bulk_density'])
        self.target_density = target_density
        self.cube_vol = cube_vol
        self.paddle_speed = paddle_speed
        # Checks if the volume is a positive number
        if cube_vol <= 0:
            raise Exception('The volume of the cube should be bigger than 0.')
        # Calculates the side of the cube
        self.cube_side = cube_vol**(1/3)
        # Checks if the cube is at least 1 mm
        if self.cube_side < 0.001:
            raise Exception('''The size of the side of the cube should have at  
                            least 1 mm, I mean, seriously? Smaller than this? ''')
        if particle_radi * 16 > self.cube_side:
            warnings.warn('''The minimum receommended size of the side of the 
                          cube is 8 times the particle size.''')

        # Defines the boundaries of the sim [m]
        self.b_x_min = 0
        self.b_x_max = 3.1 * self.cube_side
        self.b_y_min = 0
        self.b_y_max = 1.1 * self.cube_side
        self.b_z_min = 0
        self.b_z_max = 3.1 * self.cube_side

        # Defines the timestep
        self.timestep = self.calc_timestep(self)

        # Number of particles in the cube
        self.n_p_cube = self.n_particles()

        # Scale factor for the sim
        self.scale_factor = self.cube_side / 0.001

        # Paddle moviment time [s]
        self.paddle_m_time = self.cube_side / self.paddle_speed
        
    def gen_parameters(self):
        '''
        Generates the parameters file for the LIGGGHTS simulation.
        '''
        pass

    def calc_results(self):
        '''
        Calculates the density of the particles based in the results
        of the simulation.
        '''
        #TODO Check if the results exists
        pass

    def output_results(self):
        '''
        Output the results of the simulation.
        '''
        #TODO generates a file with the results
        #TODO print the results on the screen
        pass

    #TODO evaluate the need to include in the arguments the keywords to 
    #TODO Young's modulus and Poisson's ratio
    #TODO evaluate the need to include vibration in the box

    # Copia os arquivos para a pasta de destino
    # Run the program
    # Get the results
    # Calculate the equivalent specific weight


################################################################################