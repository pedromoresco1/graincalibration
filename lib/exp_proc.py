'''
This is  the main file for the other classes, 
it'll have all the necessary standard methods.
'''
import os
import shutil
import subprocess

################################################################################

class ExpProc:

    def __init__(self, particle_radi: float, particle_ym: float, 
                 particle_pc: float, liggghts_path: str, output_folder: str, 
                 rname: str, out_format: str, cli: bool = True):
        self.particle_radi = particle_radi
        self.particle_ym = particle_ym
        self.particle_pc = particle_pc
        self.liggghts_path = liggghts_path
        self.output_folder = output_folder
        self.name = rname
        self.out_format = out_format
        self.cli = cli
        self.template_dir = ''

    def run_case(self):
        '''
        Run the case using LIGGGHTS.
        '''
        # Checks LIGGGHTS path
        if not os.path.exists(self.liggghts_path):
            raise Exception('The path to the LIGGGHTS executable is not avaible.')
        # Run the simulation and capture the output
        cli_messages = subprocess.run([self.liggghts_path, '-in', ], 
                        shell = True, capture_output = True)

    def copy_files(self):
        '''
        Copies the templates to the destination folder.
        '''
        # Checks if the destination directorie exists
        if not os.path.exists(self.output_folder):
            # Creates the destination folder
            os.mkdir(self.output_folder)
        # Copies the source folder
        shutil.copytree(self.template_dir, self.output_folder, dirs_exists=True)

    def calc_timestep(self) -> float:
        '''
        Calculates the timestep based on Rayleigh.
        '''
        shear_moudulus = self.particle_ym / (2 * (1 + self.particle_pc))
        # dt_r = PI*r*sqrt(rho/G)/(0.1631*nu+0.8766),
        ts_raleigh = math.pi * self.particle_radi * \
                     math.sqrt(self.target_density / shear_moudulus) \
                     / (0.1631 * self.particle_pc + 0.8766)

        c_ts = 0.2 * ts_raleigh
        # Find the significant number
        ts_log10 = math.floor(math.log10(c_ts))
        c_timestep = math.pow(10, ts_log10)
        return c_timestep

    def tmp_study_name(self):
        '''
        Creates a temporary name for study.
        '''
        # Gets the current date and time
        ct_str = datetime.datetime.now().strftime('%Y-%m-%d-%H-%M')
        # Assembles the study name
        st_str = f'study_{dt_str}'
        return st_str