'''
This is the CLI interface to the Grain Calibration.
'''

import click

from lib.bulk_density import BulkDensity

################################################################################

@click.group()
@click.help_option('--help', '-h')
def cli():
    '''
    For help refer to each command.
    '''

################################################################################
@click.command()
@click.option('--target_density', '-td', type=click.IntRange(min=0), 
              required=True,
              help='Targeted aparent density of the particles[kg/m3].')
@click.option('--cube_vol', '-cv', type=click.IntRange(min=0), required=True,
              help='Volume of the cube to deposit the particles[m3].')
@click.option('--paddle_speed', '-ps', type=click.IntRange(min=0), 
              required=False, default=0.2, show_default=True, 
              help='Speed of the level paddle[m/s].')
@click.option('--particle_radi', '-pr', type=click.IntRange(min=0), 
              required=True,help='Radius of the particles[m].')
@click.option('--particle_ym', '-pym', type=click.IntRange(min=0), 
              required=False, default=1e8, show_default=True, 
              help='Module of Young of the particles[N/m].')
@click.option('--particle_pc', '-pc', type=click.IntRange(min=0,max=1), 
              required=False, default=0.25, show_default=True, 
              help='Poisson coefficient of the particles.')
@click.option('--liggghts_path', '-lp', type=click.Path(exists=True), 
              required=True, help='Path or name to LIGGGHTS executable.')
@click.option('--output_folder', '-of', type=click.Path(exists=True), 
              required=True, help='Folder to store the results.')
@click.option('--study_name', '-sn', type=str, required=False, default='', 
               help='Name of the study.')
@click.option('--output_format', '-of', type=click.Choice(['vtk','liggghts'], 
              case_sensitive=True), show_choices=True, required=False, 
              default='vtk', show_default=True, 
              help='Format of the output results.')
def bulkdensity(target_density, cube_vol, paddle_speed, particle_radi, 
                particle_ym, particle_pc, liggghts_path, output_folder, 
                study_name, output_format):
    '''
    This test fills a container with particles and calculates their density for 
    a given bulk density.
    '''
    # Creates the object
    bdt = BulkDensity(target_density=target_density, cube_vol=cube_vol, 
                      paddle_speed=paddle_speed, particle_radi=particle_radi, 
                      particle_ym=particle_ym, particle_pc=particle_pc, 
                      liggghts_path=liggghts_path, output_folder=output_folder, 
                      rname=study_name, output_format=output_format, cli=True)
    # Copies the tempalte study files
    bdt.copy_files()
    # Generates the LIGGGHTS parameters file
    bdt.gen_parameters()
    # Runs the case
    bdt.run_case()
    # Calculates the results
    bdt.calc_results()
    # Gets the results
    bdt.output_results()
    
################################################################################

cli.add_command(bulkdensity)

if __name__ == '__main__':
    cli()