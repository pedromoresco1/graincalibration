# Grain Calibration Workbench

## Intro

This is a workbench to help users in the process of calibration of bulk materials for computational simulations using the discrete element method(DEM) with [LIGGGHTS-PUBLIC](https://github.com/CFDEMproject/LIGGGHTS-PUBLIC) solver.
In DEM simulations the process of calibration means that a laboratory test is reproduced in the software in order to find the correct simulations parameters to match the real world behavior of the material.

## Calibration Relevance
The calibration procedure is very important to ensure the simulations have a valid and usable results since:
- The DE method is a numerical abstraction of the real phenomena and not all the parameters used by the contact algorithms have a real life equivalent and neither all the softwares will have identical implementations;
- The materials used in the simulations have origin dependant properties, meaning that given two samples of the same bulk material with different sources two indepent sets of parameters will have to be used;
- The simulation parameters(model parameters) will often be different from their real counterparts in order to reproduce macro behaviors of the bulk material, since model parameters are usually in the micro scale(particle-based values);
- The technique of coarse-graining(particle upscaling) increase the diameter of the particles to reduce simulation time, this fundamentally changes the macro and micro behavior of the material requiring the tunning of the input parameters in order to give usable results.

## Calibration procedures

Above the present calibration experiments are shown.

### Bulk density test

## Notes
Be aware that this project for now offers support out-of-the-box only for free flowing, dry particles. The use of cohesive and adhesive particles is avaible through customization.

The author does not accounts for the righteousness of the results nor the right usage of the application, this software is provided "as is" under the MIT.